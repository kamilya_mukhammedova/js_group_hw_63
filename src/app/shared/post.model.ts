export class Post {
  constructor(
    public id: string,
    public date: number,
    public description: string,
    public title: string,
  ) {}
}
