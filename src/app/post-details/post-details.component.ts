import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Post } from '../shared/post.model';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-post-details',
  templateUrl: './post-details.component.html',
  styleUrls: ['./post-details.component.css']
})
export class PostDetailsComponent implements OnInit {
  post: Post | null = null;
  postId = '';

  constructor(
    private route: ActivatedRoute,
    private http: HttpClient,
    private router: Router
    ) { }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.postId = params['id'];
      this.getPost(this.postId);
    });
  }

  getPost(idPost: string) {
    this.http.get<Post>(`https://kamilya-61357-default-rtdb.firebaseio.com/myPosts/${idPost}.json`)
      .pipe(map(result => {
        return result;
      }))
      .subscribe(post => {
        this.post = post;
      });
  }

  deletePost() {
    this.http.delete(`https://kamilya-61357-default-rtdb.firebaseio.com/myPosts/${this.postId}.json`)
      .subscribe(() => {
        void this.router.navigate(['/']);
      });
  }
}
